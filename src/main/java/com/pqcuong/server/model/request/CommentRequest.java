package com.pqcuong.server.model.request;

public class CommentRequest {
    private int id_comment;
    private int id_news;
    private int id_user;
    private String content;
    private int numberlike;

    public int getId_comment() {
        return id_comment;
    }

    public void setId_comment(int id_comment) {
        this.id_comment = id_comment;
    }

    public int getId_news() {
        return id_news;
    }

    public void setId_news(int id_news) {
        this.id_news = id_news;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getNumberlike() {
        return numberlike;
    }

    public void setNumberlike(int numberlike) {
        this.numberlike = numberlike;
    }
}
