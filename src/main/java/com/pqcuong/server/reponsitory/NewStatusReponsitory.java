package com.pqcuong.server.reponsitory;

import com.pqcuong.server.model.database.NewsStatus;
import com.pqcuong.server.model.response.NewDetailResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface NewStatusReponsitory extends JpaRepository<NewsStatus, Integer> {
    List<List<NewsStatus>> getAllInfoNewById  = new ArrayList<List<NewsStatus>>();
    @Query(nativeQuery = true,
            value = "SELECT * FROM news_status WHERE " + " id_user=:id_user")
    List<NewsStatus> getAllNewsStatusByUser(@Param(value = "id_user")int id_user);

    @Query(nativeQuery = true,
            value = "SELECT * FROM news_status WHERE id_new=:id_new")
    NewsStatus findOneByIdNew(int id_new);
}
