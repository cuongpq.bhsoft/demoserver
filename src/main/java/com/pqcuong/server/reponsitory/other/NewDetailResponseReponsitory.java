package com.pqcuong.server.reponsitory.other;

import com.pqcuong.server.model.response.NewDetailResponse;
import com.pqcuong.server.model.response.NewStatusResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface NewDetailResponseReponsitory extends JpaRepository<NewDetailResponse,Integer> {
    @Query(nativeQuery = true, value = "SELECT news_status.*," +
            "user_profile.falname, user_profile.avatar FROM news_status " +
            "JOIN user_profile ON news_status.id_user =user_profile.id_user WHERE news_status.id_new=:id_new")
    NewDetailResponse findOneByIdNew(@Param(value = "id_new")int id_new);

    @Query(nativeQuery = true, value = "SELECT news_status.*," +
            "user_profile.falname, user_profile.avatar FROM news_status " +
            "JOIN user_profile ON news_status.id_user =user_profile.id_user " +
            "WHERE news_status.id_user=:id_user ORDER BY id_new DESC LIMIT 1")
    NewDetailResponse findOneNewByIdUser(@Param(value = "id_user")int id_user);
}
