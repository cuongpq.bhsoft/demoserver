package com.pqcuong.server.reponsitory.other;

import com.pqcuong.server.model.response.BaseResponse;
import com.pqcuong.server.model.response.VideoResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface VideoResponseReponsitory extends JpaRepository<VideoResponse, Integer> {
    @Query(nativeQuery = true,value = "select news_video.id_video ,news_video.comment_status," +
            "news_video.like_status ,news_video.content ,news_video.link_video ," +
            "news_video.time_upload ,user_profile.falname ,user_profile.avatar ,friend.id_user,friend.id_ufriend " +
            "from news_video " +
            "join user_profile on news_video.id_user=user_profile.id_user " +
            "join friend on friend.id_ufriend = news_video.id_user " +
            "where friend.id_user=:id_user")
    List<VideoResponse> getAllVideoById(@Param(value = "id_user")int id_user);
}
