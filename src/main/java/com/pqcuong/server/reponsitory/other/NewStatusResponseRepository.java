package com.pqcuong.server.reponsitory.other;

import com.pqcuong.server.model.request.NewStatusRequest;
import com.pqcuong.server.model.response.NewStatusResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NewStatusResponseRepository extends JpaRepository<NewStatusResponse, Integer> {
//    private int id;
//    private int id_user;
//    private String username;
//    private String avatar;
//    private String content;
//    private String link_image;
//    private int like_status;
//    private int comment_status;
//    private String timenew;
    @Query(nativeQuery = true, value = "select news_status.id_new, news_status.content, " +
            "news_status.link_image,news_status.like_status,news_status.comment_status, " +
            "news_status.timenew, friend.id_user, friend.id_ufriend, user_profile.avatar, " +
            "user_profile.falname \n" +
            "from news_status \n" +
            "join friend on news_status.id_user=friend.id_ufriend \n" +
            "join user_profile on friend.id_ufriend=user_profile.id_user \n" +
            "where friend.id_user=:id_user " +
            "ORDER BY id_new DESC")
    List<NewStatusResponse> findAllStatusNews(@Param(value = "id_user")int id_user);

}
