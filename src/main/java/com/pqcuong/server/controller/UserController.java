package com.pqcuong.server.controller;

import com.pqcuong.server.model.database.Friend;
import com.pqcuong.server.model.database.UserProfile;
import com.pqcuong.server.model.request.*;
import com.pqcuong.server.model.response.BaseResponse;
import com.pqcuong.server.reponsitory.FriendReponsitory;
import com.pqcuong.server.reponsitory.UserProfileReponsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserProfileReponsitory userProfileReponsitory;
    @Autowired
    private FriendReponsitory friendReponsitory;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @PostMapping("/api/login")
    public Object login(@RequestBody LoginRequest request){
        UserProfile userProfile = userProfileReponsitory.findOneByUsernamePassword(request.getUsername());

        if (userProfile==null){
            return new BaseResponse(false,"Your username is not correct");
        }
        if (!passwordEncoder.matches(request.getPassword(),userProfile.getPassword())){
            return new BaseResponse(false, "your password is not correct");
        }

        return new BaseResponse(userProfile);
    }
    @PostMapping("/api/register")
    public Object register(@RequestBody RegisterRequest request){
        UserProfile userProfile = userProfileReponsitory.findOneByUsernamePassword(request.getUsername());
        if (userProfile!=null){
            return new BaseResponse(false,"This user name is exit!");
        }
        userProfile= new UserProfile();
        userProfile.setAvatar(request.getAvatar());
        userProfile.setUsername(request.getUsername());
        userProfile.setPassword(passwordEncoder.encode(request.getPassword()));
        userProfile.setDob(request.getDob());
        userProfile=userProfileReponsitory.save(userProfile);
        UserProfile maxId = userProfileReponsitory.getMaxId();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        Friend friend = new Friend();
        friend.setId_friend(maxId.getId_user());
        friend.setId_user(maxId.getId_user());
        friend.setTimeadd(dateFormat.format(date));
        friendReponsitory.save(friend);
        return new BaseResponse(userProfile);

    }
    @PostMapping(value = "/api/getUserById")
    public Object getUserById(@RequestBody NewStatusRequest request){
        UserProfile userProfile = userProfileReponsitory.findOneByUserId(request.getId_user());
        if (userProfile==null){
            return new BaseResponse(false,"Your user doesn't exit!");
        }
        return  new BaseResponse(userProfile);
    }
    @PostMapping(value = "/api/getAllUser")
    public Object getAllUser(){
        List<UserProfile> userProfile = userProfileReponsitory.getAllUser();
        if (userProfile==null||userProfile.size()==0){
            return new BaseResponse(false,"Don't have a user in data");
        }

        return  new BaseResponse(userProfile);
    }
    @RequestMapping(value = "/api/uploadImage",method = RequestMethod.POST,consumes = "multipart/form-data")
    public Object upLoadImage(@RequestParam("file")MultipartFile file, int id_user){
        DateTimeFormatter dtf  = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String currentTime = now.toString().replace("/","");
        currentTime = currentTime.toString().replace(":","");
        currentTime = currentTime.toString().replace(" ","");
        currentTime = currentTime.toString().replace("-","");
        currentTime = currentTime.toString().replace(".","");
        String fileName = currentTime+file.getOriginalFilename();
        long fileSize = (long) file.getSize()/1024;
        File convertFile =  new File("C:\\xampp\\htdocs\\android\\upload\\avatar\\"+fileName);
        try {
            convertFile.createNewFile();
            FileOutputStream fout = new FileOutputStream(convertFile);
            fout.write(file.getBytes());
            fout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        UserProfile userProfile = userProfileReponsitory.findById(id_user).get();
        if (userProfile==null){
            return null;
        }
        userProfile.setAvatar("http://192.168.167.100:81/android/upload/avatar/"+fileName);
        UserProfile updateAvatar = userProfileReponsitory.save(userProfile);
        return new BaseResponse(updateAvatar);
    }
    @PostMapping(value = "/api/updateById")
    public Object updateById(@RequestBody UpdateUserRequest request){
        UserProfile userProfile = userProfileReponsitory.findById(request.getId_user()).get();
        if (request.getFalname()!=null && !request.getFalname().equals("")){
            userProfile.setFalname(request.getFalname());
        }
        if (request.getDob()!=null && !request.getDob().equals("")){
            userProfile.setDob(request.getDob());
        }
        if (request.getPhone()!=null && !request.getPhone().equals("")){
            userProfile.setPhone(request.getPhone());
        }
        if (request.getEmail()!=null && !request.getEmail().equals("")){
            userProfile.setEmail(request.getEmail());
        }
        if (request.getCountry()!=null && !request.getCountry().equals("")){
            userProfile.setCountry(request.getCountry());
        }
        UserProfile updateUserProfile = userProfileReponsitory.save(userProfile);
        return new BaseResponse(updateUserProfile);
    }
}
