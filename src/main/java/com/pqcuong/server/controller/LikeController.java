package com.pqcuong.server.controller;

import com.pqcuong.server.model.database.NewLike;
import com.pqcuong.server.model.database.NewsStatus;
import com.pqcuong.server.model.request.LikeRequest;
import com.pqcuong.server.model.response.BaseResponse;
import com.pqcuong.server.model.response.LikeResponse;
import com.pqcuong.server.model.response.NewDetailResponse;
import com.pqcuong.server.model.response.NewStatusResponse;
import com.pqcuong.server.reponsitory.LikeReponsitory;
import com.pqcuong.server.reponsitory.NewStatusReponsitory;
import com.pqcuong.server.reponsitory.other.LikeResponseReponsitory;
import com.pqcuong.server.reponsitory.other.NewDetailResponseReponsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
public class LikeController {
    @Autowired
    private LikeResponseReponsitory likeResponseReponsitory;
    @Autowired
    private LikeReponsitory likeReponsitory;
    @Autowired
    private NewStatusReponsitory newStatusReponsitory;
    @PostMapping(value = "/api/getNewLike")
    public Object getNewLikeByIdNew(@RequestBody LikeRequest request){
        List<LikeResponse> likeResponse = likeResponseReponsitory.findAllLikeById(request.getId_new());
        if (likeResponse==null||likeResponse.size()==0){
            return new BaseResponse(false,"This new don't have like");
        }
        return new BaseResponse(likeResponse);
    }
    @PostMapping(value = "/api/getAllLike")
    public Object getAllLike(){
        List<LikeResponse> likeResponse = likeResponseReponsitory.findAllLike();
        if (likeResponse==null||likeResponse.size()==0){
            return new BaseResponse(false,"This new don't have like");
        }
        return new BaseResponse(likeResponse);
    }
    @PostMapping(value = "/api/addNewLike")
    public Object addNewLike(@RequestBody LikeRequest request){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        LocalDateTime localDateTime = LocalDateTime.now();
        NewsStatus newsStatus = newStatusReponsitory.findOneByIdNew(request.getId_new());
        int numberLike = newsStatus.getLike_status();
        List<NewLike> newLike = likeReponsitory.findAllLikeByIdNew(request.getId_new());
        NewLike newLikes = new NewLike();
        newLikes.setId_new(request.getId_new());
        newLikes.setId_user(request.getId_user());
        newLikes.setTime_like(localDateTime.toString());
        for (NewLike like : newLike) {
            if (like.getId_new()==request.getId_new()&& like.getId_user()==request.getId_user()){
                numberLike--;
                updateLike(numberLike,request);
                likeReponsitory.deleteById(like.getId_like());
                return new BaseResponse(true,"Remove like success!");
            }
        }
        numberLike++;
        updateLike(numberLike,request);
        likeReponsitory.save(newLikes);
        return new BaseResponse(newLike);
    }
    public void updateLike(int numberLike,@RequestBody LikeRequest request){
        NewsStatus newsStatus = newStatusReponsitory.findOneByIdNew(request.getId_new());
        newsStatus.setLike_status(numberLike);
        newStatusReponsitory.save(newsStatus);
    }
}
