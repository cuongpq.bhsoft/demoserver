package com.pqcuong.server.controller;

import com.pqcuong.server.model.request.VideoRequest;
import com.pqcuong.server.model.response.BaseResponse;
import com.pqcuong.server.model.response.VideoResponse;
import com.pqcuong.server.reponsitory.other.VideoResponseReponsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class VideoController{
    @Autowired
    private VideoResponseReponsitory videoResponseReponsitory;
    @PostMapping(value = "/api/getAllVideoById")
    public Object getAllVideoById(@RequestBody VideoRequest request){
        List<VideoResponse> videoResponse = videoResponseReponsitory.getAllVideoById(request.getId_user());
        if (videoResponse==null||videoResponse.size()==0){
            return new BaseResponse(false,"Don't have any video");
        }
        return new BaseResponse(videoResponse);

    }
}
