package com.pqcuong.server.controller;

import com.pqcuong.server.model.database.Notification;
import com.pqcuong.server.model.request.NewStatusRequest;
import com.pqcuong.server.model.response.BaseResponse;
import com.pqcuong.server.model.response.NotificationResponse;
import com.pqcuong.server.reponsitory.NotificationReponsitory;
import com.pqcuong.server.reponsitory.other.NotificationResponseReponsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class NotificationController {
    @Autowired
    private NotificationReponsitory notificationReponsitory;
    @Autowired
    private NotificationResponseReponsitory notificationResponseReponsitory;

    @PostMapping("/api/getAllNotifiByIdUser")
    public Object getAllNotificationByIdUser(@RequestBody NewStatusRequest request){
        List<NotificationResponse> notification = notificationResponseReponsitory.findAllNotifiByIdUser(request.getId_user());
        if (notification==null||notification.size()==0){
            return new BaseResponse(false,"you don't have notification");
        }
        return new BaseResponse(notification);
    }
}
