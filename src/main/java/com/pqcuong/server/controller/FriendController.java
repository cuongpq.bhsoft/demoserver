package com.pqcuong.server.controller;

import com.pqcuong.server.model.database.Friend;
import com.pqcuong.server.model.database.UserProfile;
import com.pqcuong.server.model.request.AddFriendRequest;
import com.pqcuong.server.model.request.DeleteFriendRequest;
import com.pqcuong.server.model.request.FriendRequest;
import com.pqcuong.server.model.response.BaseResponse;
import com.pqcuong.server.model.response.FriendResponse;
import com.pqcuong.server.reponsitory.FriendReponsitory;
import com.pqcuong.server.reponsitory.UserProfileReponsitory;
import com.pqcuong.server.reponsitory.other.FriendResponseReponsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
public class FriendController {
    @Autowired
    private FriendReponsitory friendReponsitory;

    @Autowired
    private UserProfileReponsitory userProfileReponsitory;

    @Autowired
    private FriendResponseReponsitory friendResponseReponsitory;

    @PostMapping("/api/getAllFriend")
    public Object getAllFriend(@RequestBody FriendRequest request) {
        List<FriendResponse> friends = friendResponseReponsitory.findAllMyFriend(request.getId_user(),request.getCheck_request());
        if (friends.size() == 0 || friends == null) {
            return new BaseResponse(false, "Your doesn't have friend!");
        }
        return new BaseResponse(friends);
    }
    @PostMapping("/api/getFriendRequest")
    public Object getFriendRequest(@RequestBody FriendRequest request) {
        List<FriendResponse> friends = friendResponseReponsitory.findAllMyRequest(request.getId_ufriend(),request.getCheck_request());
        if (friends.size() == 0 || friends == null) {
            return new BaseResponse(false, "Your doesn't have friend!");
        }
        return new BaseResponse(friends);
    }
    @PostMapping("/api/acceptRequest")
    public Object acceptRequest(@RequestBody FriendRequest request){
        DateFormat dateFormat = new SimpleDateFormat("yyy/MM/dd HH:mm:ss");
        Date date = new Date();
        Friend friend = friendReponsitory.findOneFriend(request.getId_ufriend(),request.getId_user());
        if (friend==null){
            return new BaseResponse(false,"add friend fail!");
        }
        friend.setCheck_request(1);
        friendReponsitory.save(friend);
        Friend ufriend = new Friend();
        ufriend.setId_user(request.getId_user());
        ufriend.setId_ufriend(request.getId_ufriend());
        ufriend.setCheck_request(1);
        ufriend.setTimeadd(dateFormat.format(date));
        friendReponsitory.save(ufriend);
        List<FriendResponse> friends = friendResponseReponsitory.findAllMyRequest(request.getId_user(),0);
        return new BaseResponse(friends);
    }
    @PostMapping("/api/removeRequest")
    public Object removeRequest(@RequestBody FriendRequest request){
        DateFormat dateFormat = new SimpleDateFormat("yyy/MM/dd HH:mm:ss");
        Date date = new Date();
        Friend friend = friendReponsitory.findOneFriend(request.getId_ufriend(),request.getId_user());
        if (friend==null){
            return new BaseResponse(false,"add friend fail!");
        }
        friend.setCheck_request(1);
        friendReponsitory.deleteById(friend.getId_friend());
        List<FriendResponse> friends = friendResponseReponsitory.findAllMyRequest(request.getId_user(),0);
        return new BaseResponse(friends);
    }

    @PostMapping("/api/addFriend")
    public Object addFriend(@RequestBody AddFriendRequest request) {
        List<Friend> friends = friendReponsitory.findAllByIdUser(request.getId_user());
        for (Friend f : friends) {
            if (f.getId_user() == request.getId_user() && f.getId_ufriend() == request.getId_ufriend()) {
                return new BaseResponse(false, "your friend is exits!");
            }
        }
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        Friend friend = new Friend();
        friend.setId_ufriend(request.getId_ufriend());
        friend.setId_user(request.getId_user());
        friend.setTimeadd(dateFormat.format(date));
        friend = friendReponsitory.save(friend);
        List<UserProfile> userProfiles = userProfileReponsitory.getUserAddFriend(request.getId_user());
        return new BaseResponse(userProfiles);
    }

    @PostMapping(value = "/api/getUserAddFriend")
    public Object getUserAddFriend(@RequestBody FriendRequest request){
        List<UserProfile> userProfiles = userProfileReponsitory.getUserAddFriend(request.getId_user());
        if (userProfiles==null||userProfiles.size()==0){
            return new BaseResponse(false,"Don't have a user in data");
        }
        return  new BaseResponse(userProfiles);
    }

    @PostMapping("/api/removeFriend")
    public Object removeFriend(@RequestBody DeleteFriendRequest request) {
        Friend friend = new Friend();
        friend.setId_friend(request.getId_friend());
        friend.setId_user(request.getId_user());
        friend.setId_ufriend(request.getId_ufriend());
        friendReponsitory.delete(friend);
        Friend friend1 = friendReponsitory.findOneFriend(request.getId_ufriend(),request.getId_user());
        if (friend1==null){
            return new BaseResponse(friend);
        }
        friendReponsitory.delete(friend1);
        List<FriendResponse> friends = friendResponseReponsitory.findAllMyFriend(request.getId_user(),1);
        if (friends.size() == 0 || friends == null) {
            return new BaseResponse(false, "Your doesn't have friend!");
        }
        return new BaseResponse(friends);
    }
}
